import { Grid } from '@mui/material';
import Typography from '@mui/material/Typography';


function ProductDescription({ productProp }) {

    return (
        <Grid container padding={2}>
            <Grid item>
                <Typography variant='h4' sx={{ fontWeight: 600 }} padding={3}>Description</Typography>
                <Grid paddingLeft={5}>
                    <Typography variant='h6' sx={{ whiteSpace: "pre-line", color: "black" }}>{productProp.description}</Typography>
                </Grid>
            </Grid>
        </Grid>
    )

}
export default ProductDescription;