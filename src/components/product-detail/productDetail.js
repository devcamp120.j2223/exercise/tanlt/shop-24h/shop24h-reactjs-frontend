import { Button, Grid, Rating } from '@mui/material';
import Typography from '@mui/material/Typography';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import { useParams } from "react-router-dom";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';


function ProductDetail({ productProp }) {
    const { productId } = useParams();
    let getCount = localStorage.getItem(productId) ?? 0
    const [count, setCount] = useState(parseInt(getCount));
    const dispatch = useDispatch();
    const [amout, setAmount] = useState(1)
    const { cart } = useSelector((reduxData) => reduxData.addReducer);

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const getBrand = (name) => {
        const myArray = name.split(" ")
        return myArray[0]
    }
    const addCartEvent = () => {
        var cartCount = parseInt(cart) + amout
        setCount(count + amout);
        localStorage.setItem(productId, count)
        for (let i = 0; i < amout; i++) {
            dispatch({
                type: "ADD_CART",
                cart: cartCount,
                id: productProp._id,
            })
        }
        localStorage.setItem("cart", cartCount)
    }
    const addAmountClick = () => {
        setAmount(amout + 1)
    }
    const removeAmountClick = () => {
        if (amout > 1) {
            setAmount(amout - 1)
        }
    }
    localStorage.setItem(productId, count)
    return (
        <Grid container sm={12} textAlign="center" margin={2} backgroundColor="white">
            <Grid sm={4} mt={10}>
                <img src={productProp.imageUrl} alt={productProp.ordernameId} width="70%" />
            </Grid>
            <Grid sm={8} textAlign="left" padding={5}>
                <Grid item padding={2}>
                    <Typography variant='h4' fontWeight={600}>{productProp.ordernameId}</Typography>
                </Grid>
                <Grid item padding={2}>
                    <Typography variant='h6' sx={{ color: "#808080" }} fontWeight={600} display="inline">Brand:</Typography>
                    <Typography variant='h6' display="inline"> {productProp.ordernameId ? getBrand(productProp.ordernameId) : null}</Typography>
                </Grid>
                <Grid item padding={2}>
                    <Typography variant='h6' sx={{ color: "#808080" }} fontWeight={600} display="inline">Rated: </Typography>
                    <Rating defaultValue={5} precision={0.5} />
                </Grid>
                {
                    productProp.buyPrice ?
                        <Grid container padding={2}>
                            <Typography variant='h5' sx={{ color: "black", textDecoration: "line-through", display: "inline", fontWeight: 600 }} paddingRight={4}>{numberWithCommas(productProp.buyPrice)} VNĐ</Typography>
                            <Typography variant='h4' sx={{ display: "inline", color: "red", fontWeight: 600 }}>{numberWithCommas(productProp.promotionPrice)} VNĐ</Typography>
                        </Grid> : null
                }
                <Grid item>
                    <Button onClick={removeAmountClick}><RemoveCircleIcon></RemoveCircleIcon></Button>
                    <Typography sx={{ display: "inline" }}>{amout}</Typography>
                    <Button onClick={addAmountClick}><AddCircleIcon></AddCircleIcon></Button>
                </Grid>
                <Grid item padding={2}>
                    <Button variant='contained' sx={{ color: "white", borderRadius: 0, backgroundColor: "black" }} onClick={addCartEvent}>Add to cart</Button>
                </Grid>
            </Grid>
        </Grid>
    )

}
export default ProductDetail;