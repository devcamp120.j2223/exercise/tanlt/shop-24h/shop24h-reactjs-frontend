import { Grid } from '@mui/material';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../FetchAPI';
import ProductCard from '../products/productCard';

function ProductRelated({ productProp }) {
    const [related, setRelated] = useState([]);

    useEffect(() => {
        fetchAPI("http://localhost:8000/products")
            .then((data) => {
                setRelated(data.data.filter(productData => productData.type._id === productProp.type._id && productData._id !== productProp._id))
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [productProp])

    return (
        <Grid container paddingTop={2}>
            <Grid sm={12}>
                <Typography variant='h4' sx={{ fontWeight: 600 }} padding={3}>Related Product</Typography>
                <Grid container sm={12} paddingTop={2} justifyContent="center">
                    {
                        related.map((product, index) => {
                            return (
                                <Grid sm={3} key={index}>
                                    <ProductCard productProp={product}></ProductCard>
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </Grid>
        </Grid>
    )

}
export default ProductRelated;