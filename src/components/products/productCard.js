import { Grid, Card, CardActionArea, CardContent } from '@mui/material';
import Typography from '@mui/material/Typography';

function ProductCard({ productProp }) {
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return (
        <Grid textAlign="center" mb={2} padding={1} sx={{ height: "100%", alignSelf: "end", textAlign: "center" }}>
            <Card sx={{ height: "100%"}}>
                <CardActionArea href={"/products/" + productProp._id} sx={{ height: "100%"}}>
                    <CardContent sx={{width:256}}>
                        <img src={productProp.imageUrl} alt={productProp.ordernameId} style={{objectFit:"fill", width:"100%", height:250}}></img>
                        <Grid margin={1}>
                            <Typography gutterBottom fontSize={15} fontWeight={600} style={{whiteSpace:"nowrap",textOverflow: "ellipsis",overflow: "hidden" }}>
                                {productProp.ordernameId}
                            </Typography>
                        </Grid>
                        <Typography sx={{ textDecoration: "line-through", display: "inline", fontWeight: 600, fontSize: 12 }}>{numberWithCommas(productProp.buyPrice)} VNĐ</Typography>
                        <Typography sx={{ display: "inline", color: "red", fontWeight: 600, fontSize: 14 }} paddingLeft={2}>{numberWithCommas(productProp.promotionPrice)} VNĐ</Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid >
    )

}
export default ProductCard;