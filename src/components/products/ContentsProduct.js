import { Container, Grid, Pagination } from '@mui/material';
import * as React from 'react';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../FetchAPI';
import { useSelector } from 'react-redux';
import ProductFilter from './productFilter';
import ProductCard from './productCard';




function Product() {

    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const [productAPI, setProduct] = useState([]);
    const [baseUrl, setBaseUrl] = useState("http://localhost:8000/products?")

    const { pageFil, url } = useSelector((reduxData) => reduxData.filterReducer)

    const changeHandler = (event, value) => {
        setPage(value)
    }
    const filterData = () => {
        setBaseUrl(url)
    }
    useEffect(() => {
        if (baseUrl === "http://localhost:8000/products?") {
            fetchAPI(baseUrl)
                .then((data) => {
                    setNoPage(Math.ceil(data.data.length / 9));
                    setProduct(data.data.slice((page - 1) * 9, page * 9));
                })
                .catch((error) => {
                    console.error(error.message)
                })
        }
        else {
            setNoPage(pageFil);
            fetchAPI(baseUrl + "limit=9&skip=" + (page - 1) * 9)
                .then((data) => {
                    if (page > noPage) { setPage(1) }
                    setProduct(data.data);
                })
                .catch((error) => {
                    console.error(error.message)
                })
        }
    }, [page, baseUrl, noPage])
    useEffect(() => {
        setBaseUrl(url);
    }, [url])

    return (
        <Container>

            <Grid textAlign="center" padding={4}>
                <Typography variant='h3'>Danh mục sản phẩm</Typography>
            </Grid>
            <Grid container sm={12} lg={12}>
                <Grid sm={3} lg={3}>
                    <ProductFilter filterProps={filterData}></ProductFilter>
                </Grid>
                <Grid sm={9} lg={9}>
                    <Grid container sm={12} lg={12} paddingTop={5} justifyContent="center">
                        {
                            productAPI.map((product, index) => {
                                return (
                                    <Grid sm={4} lg={4} key={index}>
                                        <ProductCard productProp={product}></ProductCard>
                                    </Grid>
                                )
                            })
                        }
                    </Grid>
                    <Grid item>
                        <Grid container mt={3} mb={3} justifyContent="flex-end">
                            <Grid item>
                                <Pagination count={noPage} defaultPage={page} onChange={changeHandler}></Pagination>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

        </Container>
    )

}
export default Product;