import { Grid, FormGroup, FormControlLabel, Checkbox, Button, TextField } from '@mui/material';
import * as React from 'react';
import Typography from '@mui/material/Typography';
import { fetchAPI } from '../FetchAPI';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';


function ProductFilter({ filterProps }) {
    const [productAPI, setProduct] = useState([]);
    const [checked, setChecked] = useState([])
    const [productName, setName] = useState("")
    const [priceMin, setPriceMin] = useState("")
    const [priceMax, setPriceMax] = useState("")
    const [clicked, setClick] = useState(true);
    const dispatch = useDispatch();

    const filterData = () => {
        setClick(!clicked)
        let categories = "";
        let nameFil = "";
        let minFil = "?minPrice=";
        let maxFil = "";
        if (checked)
            for (var i = 0; i < checked.length; i++) {
                categories = categories + "&type=" + checked[i];
            }
        if (productName) {
            nameFil = "&name=" + productName;
            console.log(productName)
        }
        if (priceMin) {
            minFil = "?minPrice=" + priceMin;
        }
        if (priceMax) {
            maxFil = "&maxPrice=" + priceMax;
        }
        fetchAPI("http://localhost:8000/products" + minFil + categories + maxFil + nameFil)
            .then((data) => {
                dispatch({
                    type: "FILTER",
                    name: productName,
                    minPrice: priceMin,
                    priceMax: priceMax,
                    typeProduct: checked,
                    pageFil: Math.ceil(data.data.length / 9),
                    url: ("http://localhost:8000/products" + minFil + categories + maxFil + nameFil + "&")
                })
                filterProps();
            })
            .catch((error) => {
                console.error(error.message)
            })
    }
    const checkCate = (value) => {
        setChecked(prev => {
            const isChecked = checked.includes(value)
            if (isChecked) {
                return checked.filter(item => item !== value)
            }
            else {
                return [...prev, value]
            }
        })
    }
    useEffect(() => {
        fetchAPI("http://localhost:8000/productTypes")
            .then((data) => {
                setProduct(data.data);
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [])
    return (
        <Grid container padding={5} sx={{ backgroundColor: "#ffffff", borderRadius: "5px"}} marginTop={6}>
            <Grid item paddingBottom={3} mb={2}>
                <TextField fullWidth label="Nhập tên sản phẩm" placeholder='Nhập tên sản phẩm' value={productName} onChange={(value) => setName(value.target.value)}></TextField>
            </Grid>
            <Grid container>
                <Typography variant='h5'>Categories</Typography>
            </Grid>
            {
                productAPI.map((product, index) => {
                    return (
                        <Grid sm={12} textAlign="center" key={index} color="black" width={200}>
                            <FormGroup>
                                <FormControlLabel control={<Checkbox onChange={() => checkCate(product._id)} />} label={product.name}/>
                            </FormGroup>
                        </Grid>
                    )
                })
            }
            <Grid container sm={12} textAlign="center" paddingTop={2} mt={2}>
                <Grid sm={12} textAlign="left" mb={2}>
                    <Typography variant='h5'>Price</Typography>
                </Grid>
                <Grid sm={6} padding={1}>
                    <TextField value={priceMin} fullWidth label="Min" placeholder='Min' onChange={(event) => setPriceMin(event.target.value)}></TextField>
                </Grid>
                <Grid sm={6} padding={1}>
                    <TextField label="Max" placeholder='Max' fullWidth value={priceMax} onChange={(event) => setPriceMax(event.target.value)}></TextField>
                </Grid>
            </Grid>
            <Grid container paddingTop={2}>
                <Button variant='contained' fullWidth onClick={filterData}>Lọc</Button>
            </Grid>
        </Grid>
    )

}
export default ProductFilter;