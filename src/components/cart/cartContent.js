import { Container, TableContainer, Box, Table, Grid, TableHead, TableRow, TableCell, TableBody, Link, Button } from '@mui/material';
import * as React from 'react';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../FetchAPI';
import { useDispatch, useSelector } from 'react-redux';
import ModalLogin from '../header/modalLogin';
import ModalOrder from './modalOrder';


function Cart() {
    const { cart, id } = useSelector((reduxData) => reduxData.addReducer);
    const { userCheck } = useSelector((reduxData) => reduxData.userReducer);
    const [total, setTotal] = useState(0)
    const [list, setList] = useState([])
    const [productAPI, setProductAPI] = useState([])
    const [openModal, setOpenModal] = useState(false)
    const [modalOrder, setModalOrder] = useState(false)

    const openModalFun = () => {
        setOpenModal(true);
    };
    const closeModal = () => {
        setOpenModal(false);
    }
    const dispatch = useDispatch();
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    function countOccurrences(arr) {
        return arr.reduce(function (a, b) {
            a[b] = a[b] + 1 || 1
            return a;
        }, {});
    }
    const addButtonClick = (id) => {
        let getCount = parseInt(localStorage.getItem(id))
        let updateCount = getCount + 1
        dispatch({
            type: "ADD",
            cart: cart + 1,
            id: id
        })
        localStorage.setItem("cart", cart + 1)
        localStorage.setItem(id, updateCount)
    }
    const orderButton = () => {
        if (userCheck) {
            setModalOrder(true);
        } else {
            openModalFun()
        }
    }
    const removeButtonClick = (paramId) => {
        let getCount = parseInt(localStorage.getItem(paramId))
        let updateCount = getCount - 1
        if (getCount > 0 && cart > 0) {
            const index = id.indexOf(paramId);
            if (index > -1) {
                id.splice(index, 1);
            }
            dispatch({
                type: "REMOVE",
                cart: cart - 1,
                id: id
            })
            localStorage.setItem("cart", cart - 1)
            localStorage.setItem(paramId, updateCount)
        }
        else {
            let newList = id.filter(ids => ids !== paramId)
            dispatch({
                type: "REMOVE_ITEM",
                id: newList,
            })
            localStorage.removeItem(paramId);
        }
    }
    useEffect(() => {
        fetchAPI("http://localhost:8000/products")
            .then((data) => {
                setProductAPI(data.data.filter(product => id.includes(product._id)))
                let arr = []
                for (var property in countOccurrences(id)) {
                    let total = countOccurrences(id)[property] * (data.data.filter(product => id.includes(product._id))).find(({ _id }) => _id === property).promotionPrice
                    let obj = {
                        id: property,
                        count: countOccurrences(id)[property],
                        price: total
                    }
                    arr.push(obj)
                }
                for (let i = 0; i > arr.length; i++) {
                    setTotal(total + arr[i].count * arr[i].price)
                }
                const sumall = arr.map(item => item.price).reduce((prev, curr) => prev + curr, 0);
                setList(arr)
                setTotal(sumall)
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [id, cart, total])

    return (
        <Container>
            <ModalOrder orderDetailProps={list} costProps={total} openProps={modalOrder} closeProps={() => setModalOrder(false)}></ModalOrder>
            <ModalLogin openProps={openModal} closeProps={closeModal}></ModalLogin>
            <Grid textAlign="center" padding={4}>
                <Typography variant='h3'>Giỏ hàng</Typography>
            </Grid>
            <Grid container sm={12}>
                <Grid sm={12} style={{overflowX:"auto"}}>
                    <TableContainer component={Paper} width="100%">
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" width="30%">Sản phẩm</TableCell>
                                    <TableCell align="center">Tên sản phẩm</TableCell>
                                    <TableCell align="center">Số lượng</TableCell>
                                    <TableCell align="center">Giá tiền</TableCell>
                                    <TableCell align="center">Thành tiền</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    productAPI.map((product, index) => {
                                        return (
                                            <TableRow>
                                                <TableCell align="center" key={index} width="30%"><img src={product.imageUrl} width="20%"></img></TableCell>
                                                <TableCell align="center" key={index}><Link underline='none' href={'/products/' + product._id}>{product.ordernameId}</Link></TableCell>
                                                <TableCell align="center" key={index}><RemoveCircleIcon onClick={() => removeButtonClick(product._id)} /> {localStorage.getItem(product._id)} <AddCircleIcon onClick={() => addButtonClick(product._id)} /></TableCell>
                                                <TableCell align="center" key={index}>{numberWithCommas(product.promotionPrice)} VNĐ</TableCell>
                                                <TableCell align="center" key={index}>{numberWithCommas(parseInt(localStorage.getItem(product._id)) * product.promotionPrice)} VNĐ</TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid sm={12} paddingTop={2}>
                    <Box component={Paper} textAlign="center" padding={4} borderRadius={0}>
                        <Typography variant='h4'>Tổng tiền {cart} sản phẩm: {numberWithCommas(total)} VNĐ</Typography>
                    </Box>
                    <Box component={Paper} textAlign="right" borderRadius={0}>
                        {userCheck ? null : <Typography variant='h6' color="error" paddingRight={2}>Login tại đây</Typography>}
                        <Grid padding={2}><Button variant="contained" color='success' onClick={orderButton}>{userCheck ? "Thanh toán" : "Login"}</Button></Grid>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    )

}
export default Cart;