import { Grid, Typography, Button, Modal, Box, TextField, Alert, Snackbar, Stack } from '@mui/material';
import { useState, useEffect } from 'react';
import { fetchAPI } from '../FetchAPI';
import { useSelector } from 'react-redux';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "auto",
    height: "auto",
    bgcolor: 'background.paper',
    border: '1px solid #000',
    p: 4,
};

function ModalOrder({ openProps, closeProps, orderDetailProps, costProps, nameProps, emailProps }) {
    const { userCheck } = useSelector((reduxData) => reduxData.userReducer);
    const [fullName, setFullName] = useState(nameProps ?? "")
    const [phone, setPhone] = useState("")
    const [email, setEmail] = useState(emailProps ?? "")
    const [address, setAddress] = useState("")
    const [city, setCity] = useState("")
    const [country, setCountry] = useState("")
    const [note, setNote] = useState("")
    const [shippedDate, setShippedDate] = useState(new Date().toISOString().split('T')[0])
    const [open, setOpen] = useState(false);
    const [openErr, setOpenErr] = useState(false);
    const [textError, setTextError] = useState("")

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    const handleClose = () => {
        setOpen(false);
        setOpenErr(false);
    };

    const vaildData = () => {
        if (!fullName) {
            setTextError("Chưa nhập họ và tên")
            return false
        }
        if (isNaN(phone)) {
            setTextError("Số điện thoại không hợp lệ")
            return false
        }
        if (email.indexOf("@") === -1) {
            setTextError("Email không hợp lệ")
            return false
        }
        if (!address) {
            setTextError("Chưa nhập địa chỉ")
            return false
        }
        if (!city) {
            setTextError("Chưa nhập thành phố")
            return false
        }
        if (!country) {
            setTextError("Chưa nhập quốc gia")
            return false
        }
        return true
    }
    const buttonConfirmClick = () => {
        if (vaildData()) {
            let body = {
                method: 'POST',
                body: JSON.stringify({
                    fullName: fullName,
                    phone: phone,
                    email: email,
                    address: address,
                    city: city,
                    country: country,
                    note: note,
                    orderDetail: orderDetailProps,
                    cost: costProps,
                    shippedDate: new Date(shippedDate).toISOString()
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            };
            fetchAPI("http://localhost:8000/customers/phone/", body)
                .then((data) => {
                    setOpen(true);
                    closeProps();
                    localStorage.clear();
                })
                .catch((error) => {
                    setOpenErr(true)
                    console.error(error.message)
                })
        }
        else {
            setOpenErr(true)
        }
    }
    useEffect(() => {
        if(userCheck){
            setFullName(userCheck.displayName);
            setEmail(userCheck.email)
        }
        console.log("run")
    }, [userCheck])

    return (
        <Grid>
            <Modal open={openProps} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Grid sm={12} textAlign="center">
                        <Typography variant='h4'>Thông tin khách hàng</Typography>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField label="Nhập họ và tên" placeholder='Nhập họ và tên' value={fullName} onChange={(e) => setFullName(e.target.value)} fullWidth></TextField>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField label="Nhập số điện thoại" placeholder='Nhập số điện thoại' value={phone} onChange={(e) => setPhone(e.target.value)} fullWidth></TextField>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField label="Nhập email" placeholder='Nhập email' value={email} onChange={(e) => setEmail(e.target.value)} fullWidth></TextField>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField label="Nhập địa chỉ" placeholder='Nhập địa chỉ' value={address} onChange={(e) => setAddress(e.target.value)} fullWidth></TextField>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <Grid sm={6} paddingRight={1}>
                            <TextField label="Tỉnh/Thành phố" placeholder='Tỉnh/Thành phố' value={city} onChange={(e) => setCity(e.target.value)} fullWidth></TextField>
                        </Grid>
                        <Grid sm={6} paddingLeft={1}>
                            <TextField label="Quốc gia" placeholder='Quốc gia' value={country} onChange={(e) => setCountry(e.target.value)} fullWidth></TextField>
                        </Grid>
                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField
                            label="Ngày giao hàng (mm/dd/yyyy)"
                            type="date"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            value={shippedDate}
                            onChange={(e) => setShippedDate(e.target.value)}
                        />

                    </Grid>
                    <Grid container sm={12} textAlign="center" paddingTop={2}>
                        <TextField label="Ghi chú" placeholder='Ghi chú' value={note} onChange={(e) => setNote(e.target.value)} fullWidth multiline></TextField>
                    </Grid>
                    <Grid sm={12} padding={2}>
                        <Typography variant='h5'>Tổng: {numberWithCommas(costProps)} </Typography>
                    </Grid>
                    <Button variant='contained' onClick={buttonConfirmClick}>Confirm</Button>
                    <Button variant='contained' color="error" onClick={closeProps}>Cancel</Button>
                </Box>
            </Modal>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Đặt hàng thành công
                </Alert>
            </Snackbar>
            <Snackbar open={openErr} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {textError}
                </Alert>
            </Snackbar>
        </Grid>
    )

}
export default ModalOrder;