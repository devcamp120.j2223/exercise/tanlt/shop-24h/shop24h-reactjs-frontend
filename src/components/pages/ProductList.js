
import { Container } from '@mui/material';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderComponent from '../header/headerComponent';
import FooterComponent from '../footer/footerComponent';
import Product from '../products/ContentsProduct';
import BreadcrumbsComponent from '../breadCrumb/breadCrumb';

const breadCrumbProducts = [{
  name: "Trang chủ",
  url: "/"
}, {
  name: "Danh mục sản phẩm",
  url: "/products"
}]
function ProductList() {
  return (
    <Container maxWidth={false} sx={{backgroundColor:"#D9D9D9"}}>
      {/* <!-- Header --> */}
      <HeaderComponent></HeaderComponent>
      <BreadcrumbsComponent breadProps={breadCrumbProducts}></BreadcrumbsComponent>
      {/* <!-- Content --> */}
      <Product></Product>
      {/* <!-- Footer --> */}
      <FooterComponent></FooterComponent>
    </Container>
  );
}

export default ProductList;
