import { Container } from '@mui/material';
import HeaderComponent from '../header/headerComponent';
import FooterComponent from '../footer/footerComponent';
import BreadcrumbsComponent from '../breadCrumb/breadCrumb';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../FetchAPI';
import { useParams } from "react-router-dom";
import ProductDetail from '../product-detail/productDetail';
import ProductDescription from '../product-detail/productDescription';
import ProductRelated from '../product-detail/productRelated';


function ProductInfo() {
    const { productId } = useParams();
    const [productAPI, setProduct] = useState({});
    const breadCrumbProducts = [{
        name: "Trang chủ",
        url: "/"
    }, {
        name: "Danh mục sản phẩm",
        url: "/products"
    },
    {
        name: productAPI.ordernameId,
        url: "/products"
    }
    ]
    useEffect(() => {
        fetchAPI("http://localhost:8000/products/" + productId)
            .then((data) => {
                setProduct(data.data);
            })
            .catch((error) => {
                console.error(error.message)
            })
        
    }, [])


    return (
        <Container maxWidth={false} sx={{backgroundColor:"#D9D9D9"}}>
            {/* <!-- Header --> */}
            <HeaderComponent></HeaderComponent>
            <BreadcrumbsComponent breadProps={breadCrumbProducts}></BreadcrumbsComponent>
            {/* <!-- Content --> */}
            <ProductDetail productProp={productAPI}></ProductDetail>
            <ProductDescription productProp={productAPI}></ProductDescription>
            <ProductRelated productProp={productAPI}></ProductRelated>
            {/* <!-- Footer --> */}
            <FooterComponent></FooterComponent>
        </Container>
    );
}

export default ProductInfo;
