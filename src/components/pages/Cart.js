import { Container } from '@mui/material';
import HeaderComponent from '../header/headerComponent';
import FooterComponent from '../footer/footerComponent';
import BreadcrumbsComponent from '../breadCrumb/breadCrumb';
import Cart from '../cart/cartContent';


function CartPage() {
    const breadCrumbProducts = [{
        name: "Trang chủ",
        url: "/"
    }, {
        name: "Giỏ hàng",
        url: "/cart"
    },
    ]
    return (
        <Container maxWidth={false} sx={{backgroundColor:"#D9D9D9"}}>
            {/* <!-- Header --> */}
            <HeaderComponent></HeaderComponent>
            <BreadcrumbsComponent breadProps={breadCrumbProducts}></BreadcrumbsComponent>
            {/* <!-- Content --> */}
            <Cart></Cart>
            {/* <!-- Footer --> */}
            <FooterComponent></FooterComponent>
        </Container>
    );
}

export default CartPage;
