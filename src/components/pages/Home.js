
import { Container } from '@mui/material';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderComponent from '../header/headerComponent';
import FooterComponent from '../footer/footerComponent';
import ContentComponent from '../content/contentComponent';


// import ContentComponent from './components/content/contentComponent';
function Home() {
  return (
    <Container  maxWidth={false} sx={{backgroundColor:"#D9D9D9"}}>
      {/* <!-- Header --> */}
      <HeaderComponent></HeaderComponent>
      {/* <!-- Content --> */}
      <ContentComponent></ContentComponent>
      {/* <!-- Footer --> */}
      <FooterComponent></FooterComponent>
    </Container>
  );
}

export default Home;
