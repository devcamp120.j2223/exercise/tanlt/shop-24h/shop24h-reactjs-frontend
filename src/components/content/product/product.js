import { Container, Grid, Typography, Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../../FetchAPI';
import ProductCard from '../../products/productCard';


function Product() {
  const [productAPI, setProduct] = useState([]);

  useEffect(() => {
    fetchAPI("http://localhost:8000/products?limit=8")
      .then((data) => {
        setProduct(data.data);
      })
      .catch((error) => {
        console.error(error.message)
      })
  }, [])



  return (
    <Container>
      {/* <!-- Lastest Product --> */}
      <Grid textAlign="center">
        <Typography variant='h5' sx={{ fontWeight: 800 }}>LASTEST PRODUCT</Typography>
      </Grid>

      {
        productAPI ?
          <Container>
            <Grid container sm={12} paddingTop={5} justifyContent="center">
              {
                productAPI.map((product, index) => {
                  return (
                    <Grid sm={3}>
                      <ProductCard productProp={product} key={index}></ProductCard>
                    </Grid>
                  )
                })
              }
            </Grid>
          </Container>
          : null
      }

      <Grid sm={12} textAlign="center" padding={5}>
        <Button variant='contained' sx={{ backgroundColor: "black", paddingLeft: 3, paddingRight: 3, color: "white", borderRadius: 0 }} href="/products">View All</Button>
      </Grid>
    </Container>
  )

}
export default Product;