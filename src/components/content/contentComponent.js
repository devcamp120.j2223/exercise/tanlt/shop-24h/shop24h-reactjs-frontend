import { Container } from '@mui/material';
import Product from './product/product';
import SimpleSlider from './silder/slider';


function ContentComponent() {
    return (
        <Container>
            <Container sx={{backgroundColor:"white", marginBottom:6,marginTop:8}}>
            <SimpleSlider></SimpleSlider>
            </Container>
            <Product></Product>
        </Container>
    )
}
export default ContentComponent;