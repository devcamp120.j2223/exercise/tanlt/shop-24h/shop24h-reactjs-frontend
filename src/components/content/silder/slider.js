import React, { Component } from "react";
import Slider from "react-slick";

export default class SimpleSlider extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            autoplay: true,
            speed: 2000,
            autoplaySpeed: 10000,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return (
            <div style={{ paddingTop: "50px", paddingBottom: "30px", color:"#808080" }}>
                <Slider {...settings}>
                    <div>
                        <div className=" row col-sm-12">
                            <div className="col-sm-6">
                                <h3 className="text-danger">Loa Razer</h3>
                                <h1 className="mt-4 mb-4 text-danger">Leviathan V2</h1>
                                <p className="mt-4 mb-4">Một trong những dòng soundbar chơi game đa trình điều khiển và loa siêu trầm có THX Spatial Audio có thể tuỳ chỉnh hiệu ứng ánh sáng, hiệu năng cùng phần mềm Razer Chroma RGB. Mang đến những trải nghiệm đắm chìm cùng hình ảnh và âm thanh, được hỗ trợ bởi Bluetooth 5.2 có độ trễ thấp để sử dụng máy tính và thiết bị di động liền mạch.</p>
                                <a href="/products/62c7b4c55cb84c46e7ec3a14" ><button className="btn pt-2 pb-2" style={{ backgroundColor: "black", paddingLeft: 35, paddingRight: 35, color: "white", borderRadius: 0 }}>SHOP NOW</button></a>
                            </div>
                            <div className="col-sm-6">
                                <img alt="silde-1" style={{ height: "500px", width:"100%" }} src="https://product.hstatic.net/1000026716/product/leviathan-768x768_b2f381ebf89846c7a0a3e94e81965d5e.png"></img>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className=" row col-sm-12">
                            <div className="col-sm-6">
                                <h3 className="text-danger">Chuột Gaming Asus </h3>
                                <h1 className="mt-4 mb-4 text-danger">TUF M4 Wireless</h1>
                                <p className="mt-4 mb-4">Được thiết kế cho các phiên chơi game kéo dài, với feet chuột được làm bằng 100% PTFE và cảm biến quang học 12.000 dpi có độ chính xác cao, cung cấp khả năng kiểm soát chính xác tăng lợi thế trong trận chiến.</p>
                                <a  href="/products/62c7b3e25cb84c46e7ec3a10"><button className="btn pt-2 pb-2" style={{ backgroundColor: "black", paddingLeft: 35, paddingRight: 35, color: "white", borderRadius: 0 }}>SHOP NOW</button></a>
                            </div>
                            <div className="col-sm-6">
                                <img alt="silde-2" style={{ height: "500px", width:"100%" }} src="https://product.hstatic.net/1000026716/product/chuot-gaming-asus-tuf-m4-wireless-gearvn-4_8b6fe751524f4b8ca8f258c1d369a76f.png"></img>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className=" row col-sm-12">
                            <div className="col-sm-6">
                                <h3 className="text-danger">Tai nghe HyperX</h3>
                                <h1 className="mt-4 mb-4 text-danger">Cloud II RED</h1>
                                <p className="mt-4 mb-4">Kingston HyperX Cloud II, thế hệ mới nhất hiện nay trong series tai nghe Cloud của Kingston và là mẫu tai nghe được nhiều người cho rằng sẽ là một đối trọng xứng đáng với hai mẫu tai nghe gaming phổ biến là Razer Kraken và Steelseries Siberia. So với người đàn em phiên bản đầu của mình thì Cloud II có thêm cho mình sound card 7.1, chất âm được cải thiện hơn cũng như nhiều màu sắc hơn cho các game thủ lựa chọn.</p>
                                <a  href="/products/62c7b0925cb84c46e7ec39f6"><button className="btn pt-2 pb-2" style={{ backgroundColor: "black", paddingLeft: 35, paddingRight: 35, color: "white", borderRadius: 0 }}>SHOP NOW</button></a>
                            </div>
                            <div className="col-sm-6">
                                <img alt="silde-3" style={{ height: "500px", width:"100%" }} src="https://product.hstatic.net/1000026716/product/cloudiired-gearvn.jpg"></img>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className=" row col-sm-12">
                            <div className="col-sm-6">
                                <h3 className="text-danger">Bàn phím cơ AKKO</h3>
                                <h1 className="mt-4 mb-4 text-danger">3084B Plus World Tour Tokyo R2</h1>
                                <p className="mt-4 mb-4">Akko World Tour Tokyo là một trong những series bàn phím được nhiều người dùng lựa chọn bởi phối màu ấn tượng, độc đáo mang ảnh hưởng từ "xứ sở hoa Anh Đào". Trong đó, sản phẩm nổi bật gần đây nhất chính là AKKO 3084B Plus World Tour Tokyo R2.</p>
                                <a href="/products/62c7b1595cb84c46e7ec39fc" ><button className="btn pt-2 pb-2" style={{ backgroundColor: "black", paddingLeft: 35, paddingRight: 35, color: "white", borderRadius: 0 }}>SHOP NOW</button></a>
                            </div>
                            <div className="col-sm-6">
                                <img alt="silde-4" style={{ height: "500px", width:"100%" }} src="https://product.hstatic.net/1000026716/product/ban-phim-co-akko-3084b-plus-world-tour-tokyo-r2-08_62f5018e8e5740849b91e8bafd429dd0.jpg"></img>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
        );
    }
}