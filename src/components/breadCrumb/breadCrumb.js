import { Container, Breadcrumbs, Link } from '@mui/material';


function BreadcrumbsComponent({ breadProps }) {
    return (
        <Container>
            <Breadcrumbs aria-label="breadcrumb" paddingTop={12} sx={{color:"blue"}}>
                {
                    breadProps.map((page, index) => {
                        return (
                            <Link key={index} underline="hover" color="inherit" href={page.url}>
                                {page.name}
                            </Link>
                        )
                    })
                }
            </Breadcrumbs>
        </Container >
    )
}
export default BreadcrumbsComponent;