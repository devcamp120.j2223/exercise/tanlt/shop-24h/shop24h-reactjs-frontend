import { Input, Grid, Typography, Button, Modal, Box } from '@mui/material';
import { useEffect } from 'react';
import GoogleIcon from '@mui/icons-material/Google';
import { auth, googleProvider } from '../../firebase';
import { useDispatch } from 'react-redux';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "500px",
    height: "auto",
    bgcolor: 'background.paper',
    border: '1px solid #000',
    p: 4,
};

function ModalLogin({ openProps, closeProps }) {
    const dispatch = useDispatch();
    const loginGoogle = () => {
        auth.signInWithPopup(googleProvider)
            .then((result) => {
                dispatch({
                    type: "CHECK",
                    userCheck: result
                })
                closeProps()
            })
            .catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        auth.onAuthStateChanged((result) => {
            if (result) {
                dispatch({
                    type: "CHECK",
                    userCheck: result
                })
            }
            else {
                dispatch({
                    type: "CHECK",
                    userCheck: undefined
                })
            }
        })
    }, [])
    return (
        <Grid>
            <Modal open={openProps} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" onBackdropClick={closeProps}>
                <Box sx={style}>
                    <Grid sm={12} textAlign="center">
                        <Button variant='contained' color="error" onClick={loginGoogle} fullWidth sx={{ borderRadius: "40px", padding: 1, textTransform: "none", fontSize: 17 }}><GoogleIcon sx={{ marginRight: 2 }} />Sign in with <Box fontWeight='bold' display='inline' paddingLeft={1}>Google</Box></Button>
                    </Grid>
                    <Grid container sm={12} textAlign="center">
                        <Grid sm={5}><Box sx={{ borderTop: 1, marginTop: 7, width: "100%" }}></Box></Grid>
                        <Grid sm={2}><Box sx={{ borderRadius: "50%", width: 50, height: 50, border: 1, marginTop: 4, marginLeft: 1.5, padding: 1, fontSize: 20, fontWeight: "bold" }} alignItems="center" alignContent="center"> or</Box></Grid>
                        <Grid sm={5}><Box sx={{ borderTop: 1, marginTop: 7, width: "100%" }}></Box></Grid>
                    </Grid>
                    <Grid sm={12} textAlign="center" mt={5}>
                        <Input placeholder='   Username' sx={{ borderRadius: "40px", padding: 1, border: 1, margin: 1 }} disableUnderline fullWidth></Input>
                    </Grid>
                    <Grid sm={12} textAlign="center">
                        <Input placeholder='   Password' sx={{ borderRadius: "40px", padding: 1, border: 1, margin: 1 }} type='password' disableUnderline fullWidth></Input>
                    </Grid>
                    <Grid sm={12} textAlign="center">
                        <Button variant='contained' color="success" fullWidth sx={{ borderRadius: "40px", padding: 1, textTransform: "none", fontSize: 17, fontWeight: "bold", margin: 1 }}>Sign in</Button>
                    </Grid>
                    <Grid sm={12} textAlign="center" mt={5}>
                        <Typography display='inline'>Don't have an account?</Typography><Typography fontWeight='bold' display='inline' paddingLeft={1} color="green">Sign up here</Typography>
                    </Grid>
                </Box>
            </Modal>
        </Grid>
    )

}
export default ModalLogin;