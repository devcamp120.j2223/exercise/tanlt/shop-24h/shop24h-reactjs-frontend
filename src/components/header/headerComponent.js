import { AppBar, Toolbar, Grid, Badge, Typography, IconButton, MenuItem, Menu, Avatar } from '@mui/material';
import { useState } from 'react';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import ModalLogin from './modalLogin';
import { auth } from '../../firebase';
import { useSelector, useDispatch } from 'react-redux';

function HeaderComponent() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [openModal, setOpenModal] = useState(false)
    const { cart } = useSelector((reduxData) => reduxData.addReducer);
    const { userCheck } = useSelector((reduxData) => reduxData.userReducer);
    const dispatch = useDispatch();


    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    }
    const loginClick = () => {
        setOpenModal(true);
        setAnchorEl(null);
    };
    const logoutClick = () => {
        auth.signOut()
            .then((result) => {
                console.log(result);
                dispatch({
                    type: "CHECK",
                    userCheck: undefined
                })
            })
            .catch((err) => {
                console.log(err);
            })
        setAnchorEl(null);
    };
    const closeModal = () => {
        setOpenModal(false);
    }
    return (
        <AppBar component="nav" >
            <ModalLogin openProps={openModal} closeProps={closeModal}></ModalLogin>
            <Toolbar sx={{ backgroundColor: "#FFAC1C" }}>
                <Grid item >
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            flexGrow: 1,
                            fontWeight: 700,
                            color: "Blue",
                            textDecoration: "none"
                        }}
                    >
                        ShopGear
                    </Typography>
                </Grid>
                <Grid sx={{ width: "100%" }} textAlign="right">
                    <IconButton aria-label="notification">
                        <NotificationsNoneOutlinedIcon sx={{ color: "black" }} />
                    </IconButton>
                    <IconButton aria-label="account">
                        {
                            <div>
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    {
                                        userCheck ? <Avatar src={userCheck.photoURL} /> : <AccountCircleOutlinedIcon sx={{ color: "black" }} />
                                    }

                                </IconButton>
                                <Menu
                                    sx={{ mt: '45px' }}
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={Boolean(anchorEl)}
                                    onClose={handleClose}
                                >
                                    {
                                        userCheck ?
                                            <>
                                                <Typography padding={2} fontWeight={600}>{userCheck.displayName}</Typography>
                                                <MenuItem onClick={logoutClick}>Logout</MenuItem>
                                            </>
                                            : <MenuItem onClick={loginClick}>Login</MenuItem>
                                    }
                                </Menu>
                            </div>
                        }
                    </IconButton>
                    <IconButton aria-label="cart">
                        <Badge color="error" badgeContent={cart}>
                            <ShoppingCartOutlinedIcon sx={{ color: "black" }} onClick={() => window.location.replace('/cart')} />
                        </Badge>
                    </IconButton>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}
export default HeaderComponent;