import { Container, Grid, Typography } from '@mui/material';
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';

function FooterComponent() {
    return (
        <Container sx={{ backgroundColor: "#FFAC1C", color:"#342826"}} maxWidth={false}>
            <Grid container sm={12} padding={5} mt={10}>
                <Grid sm={3} padding={2}>
                    <Typography variant='h6' sx={{ color: "black", fontWeight: 600 }} mb={2}>PRODUCTS</Typography>
                    <Typography mb={1}>Tai nghe</Typography>
                    <Typography mb={1}>Bàn Phím</Typography>
                    <Typography mb={1}>Chuột Gaming</Typography>
                    <Typography mb={1}>Loa</Typography>
                </Grid>
                <Grid sm={3} padding={2}>
                    <Typography variant='h6' sx={{ color: "black", fontWeight: 600 }} mb={2}>SERVICES</Typography>
                    <Typography mb={1}>Help Center</Typography>
                    <Typography mb={1}>Product Help</Typography>
                    <Typography mb={1}>Warranty</Typography>
                    <Typography mb={1}>Order Status</Typography>
                </Grid>
                <Grid sm={3} padding={2}>
                    <Typography variant='h6' sx={{ color: "black", fontWeight: 600 }} mb={2}>SUPPORT</Typography>
                    <Typography mb={1}>Email: luuthanhtan0812@gmail.com</Typography>
                    <Typography mb={1}>Contact Us: 0799692221</Typography>
                </Grid>
                <Grid sm={3} textAlign="center">
                    <Typography variant='h4' sx={{ color: "black", fontWeight: 700 }} m={3}>ShopGear</Typography>
                    <Grid m={2}>
                        <FacebookOutlinedIcon />&nbsp;
                        <InstagramIcon />&nbsp;
                        <YouTubeIcon />&nbsp;
                        <TwitterIcon />&nbsp;
                    </Grid>
                </Grid>
            </Grid>
        </Container >
    )
}
export default FooterComponent;