import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDxtOM0SQ6pEqXSLvr4qS7Q8Ei6INF4jv4",
    authDomain: "fir-authentication-c8a99.firebaseapp.com",
    projectId: "fir-authentication-c8a99",
    storageBucket: "fir-authentication-c8a99.appspot.com",
    messagingSenderId: "909922511658",
    appId: "1:909922511658:web:b7ba1a0a64a736644ecc53"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider()