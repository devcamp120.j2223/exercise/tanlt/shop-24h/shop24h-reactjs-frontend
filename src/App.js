import "./App.css"
import { Routes, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import ProductInfo from './components/pages/ProductInfo';
import ProductList from './components/pages/ProductList';
import CartPage from "./components/pages/Cart";



function App() {


  return (
    <Routes>
      <Route exact path="/" element={<Home></Home>}></Route>
      <Route exact path="/products" element={<ProductList></ProductList>}></Route>
      <Route exact path="/products/:productId" element={<ProductInfo></ProductInfo>}></Route>
      <Route exact path="/cart" element={<CartPage></CartPage>}></Route>
    </Routes>
  );
} 

export default App;
