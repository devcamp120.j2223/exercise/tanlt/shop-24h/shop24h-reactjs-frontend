
const initialState = {
    userCheck: undefined
}

const userCheckEvent = (state = initialState, action) => {
    switch (action.type) {
        case "CHECK":
            console.log(action.userCheck)
            return {
                userCheck: action.userCheck
            }
        default:
            return state
    }
}


export default userCheckEvent