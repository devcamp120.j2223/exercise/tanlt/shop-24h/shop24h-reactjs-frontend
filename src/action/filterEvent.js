
const initialState = {
    name: "",
    minPrice:"",
    maxPrice:"",
    typeProduct: [],
    pageFil: 1,
    url:"http://localhost:8000/products?"
}

const filterEvent = (state = initialState, action) => {
    switch (action.type) {
        case "FILTER":
            return {
                name: action.name,
                minPrice: action.minPrice,
                maxPrice: action.maxPrice,
                typeProduct: action.typeProduct,
                pageFil: action.pageFil,
                url: action.url
            }
        default:
            return state
    }
}

export default filterEvent