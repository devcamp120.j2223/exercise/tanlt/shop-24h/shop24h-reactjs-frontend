
import { createStore, combineReducers } from "redux";
import addCartEvent from "../action/addCartEvent";
import filterEvent from "../action/filterEvent";
import userCheckEvent from "../action/userCheckEvent";

const appReducer = combineReducers({
    filterReducer: filterEvent,
    addReducer: addCartEvent,
    userReducer: userCheckEvent
});

const store = createStore(
    appReducer,
    undefined,
    undefined
);

export default store;